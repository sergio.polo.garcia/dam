// tres-v2: Que se vean las celdas

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define N 3

void pintar (unsigned t[N][N]){
    printf ("\n");
    for (unsigned f=0; f<N; f++) {
        printf ("\t");
        for (unsigned c=0; c<N; c++){
            printf (" %c ", t[f][c] == 0 ? ' ' : t[f][c] + '0');
            if (c < N-1)
                printf ("│");
        }
        printf ("\n");

        // Imprimir una linea horizontal
        printf ("\t");
        if (f < N-1)
            for (unsigned c=0; c<N; c++) {
                printf ("───");
                if (c < N-1)
                    printf ("┼");
            }
        printf ("\n");
    }

    printf ("\n");
}

void fingir_datos(unsigned t[N][N]) {
    t[0][0] = t[1][0] = t[2][0] = 1;

    t[2][1] = 2;
 
}


void comprobar( unsigned t[N][N]) {

   for (int i = 0; i < 3; ++i)
   {

        // Comprobación vertical

        if ((t[i][0] == 1) && (t[i][1] == 1) && (t[i][2] == 1)){
            printf("Jugador 1 gana 👏 \n\n");}
        else{
            if ((t[i][0] == 2) && (t[i][1] == 2) && (t[i][2] == 2)){
            printf("Jugador 2 gana 👏 \n\n");}   
        }

        // Comprobación horizontal

        if ((t[0][i] == 1) && (t[1][i] == 1) && (t[1][i] == 1)){
            printf("Jugador 1 gana 👏 \n\n");
        }
        else{
            if ((t[0][i] == 2) && (t[1][i] == 2) && (t[1][i] == 2)){
                printf("Jugador 2 gana 👏 \n\n");
            }
        }
    }
}




int main (int argc, char *argv[]) {

    unsigned tablero[N][N];

    /* Inicialización */
    bzero (tablero, sizeof(tablero));
    fingir_datos(tablero);

    pintar(tablero);

    comprobar(tablero);
 

    return EXIT_SUCCESS;
}

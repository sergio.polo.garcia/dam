//Ejercicio 1 2ºCFGS Sergio Polo
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX 20

//Variable declaration
char mas_larga[MAX] = "";


//Print result
void imprimir(){
	printf("%s \n",mas_larga);
};


int main(int argc, char *argv[]){

	for (int i = 1; i < argc; ++i){

		if (strlen(argv[i]) > strlen(mas_larga))			//Comparar que cadena es mas larga		
			strcpy(mas_larga, argv[i]);						//Copiar cadenas
	}
	
	imprimir();

	return EXIT_SUCCESS;
}
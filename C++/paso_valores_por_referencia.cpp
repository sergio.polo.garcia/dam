/* paso_valores_por_referencia c++ v1 Sergio Polo Garcia*/


#include <iostream>

using namespace std;

void preguntar(int &num1, int &num2){

	cout<<"Dime 2 numeros: "<<endl;
	cin>>num1>>num2;

}

void calcular (int x, int y, int &suma, int &producto){

	suma = x+y;
	producto = x*y;

}

void imprimir_resultado (int &suma, int &producto){

	cout<<"El resultado de la suma es: "<<suma<<endl;
	cout<<"El producto es: "<<producto<<endl;
}




int main(int argc, char const *argv[])
{
	int num1=0, num2=0, suma = 0, producto = 0;

	preguntar(num1, num2);
	calcular(num1, num2, suma, producto);
	imprimir_resultado(suma, producto);
	
	return EXIT_SUCCESS;
}
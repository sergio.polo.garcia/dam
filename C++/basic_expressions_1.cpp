/* Basic expressions c++ v1 Sergio Polo Garcia */ 

#include <iostream>
#include <math.h>

using namespace std;


int main(int argc, char const *argv[])
{
    
    // Ejercicio 1

        float num1_1, num2_1, solucion1 = 0;        //Igualamos solucion a 0 para limpiar bits residuales

        //Recogida de datos
        cout<<"Dime el primer valor: "; cin>>num1_1;
        cout<<"Dime el segundo valor: "; cin>>num2_1;
    
        //Calculos 
        solucion1 = (num1_1 / num2_1) + 1;

        //Salida de datos
        cout<<"\n#########\n\n";

        cout.precision(3);                          //Limita el resultado a 2 decimales redondeando
        cout<<"La solución es: "<<solucion1<<endl;


    // Ejercicio 2

        float num1_2, num2_2, num3_2, num4_2, solucion2 = 0;

        //Recogida de datos
        cout<<"Dime el primer valor: "; cin>>num1_2;
        cout<<"Dime el segundo valor: "; cin>>num2_2;
        cout<<"Dime el tercer valor: "; cin>>num3_2;
        cout<<"Dime el cuarto valor: "; cin>>num4_2;
    
        //Calculos 
        solucion2 = (num1_2 + num1_2) / (num3_2 + num4_2);

        //Salida de datos
        cout<<"\n#########\n\n";

        cout.precision(3);
        cout<<"La solucion es: "<<solucion2<<endl;  


    // Intercambio de valores entre dos variables

        int var_1, var_2, var_aux;

        //Recogida de datos
        cout<<"Dime el valor de la primera variable: "; cin>>var_1;
        cout<<"Dime el valor de la segunda variable: "; cin>>var_2;

        //Calculos/algoritmo
        var_aux = var_1;
        var_1 = var_2;
        var_2 = var_aux;

        //Salida de datos
        cout<<"\n";
        cout<<"Ahora la primera variable tiene como valor: "<<var_1<<endl;
        cout<<"Y la segunda: "<<var_2<<endl; 


    // Ejercicio 3

        float nota_practica, nota_teorica, nota_participacion, nota_final = 0;

        //Recogida de datos
        cout<<"Introduce la nota practica del alumno: ";            cin>>nota_practica;
        cout<<"Introduce la nota teorica del alumno: ";             cin>>nota_teorica;
        cout<<"Introduce la nota de participacion del alumno: ";    cin>>nota_participacion;

        //Calculos 
        nota_practica *= 0.30;                                              //calculamos el 30%
        nota_teorica *= 0.60;                                               //calculamos el 60%
        nota_participacion *= 0.10;                                         //calculamos el 10%

        nota_final = nota_practica + nota_teorica + nota_participacion;     //media de las 3 notas con losn porcentajes ya hechos

        //Salida de datos
        cout<<"\n ############## \n\n";
        cout.precision(3);
        cout<<"La nota final de alumno es: "<<nota_final<<endl;


    //Calculo de racices cuadradas, para ello haremos uso de la libreria: <math.h>

        float raiz;

        //Recogida de datos
        cout<<"Dime un numero al que calcularle la raíz cuadrada: ";    cin>>raiz;

        //Calculos
        raiz = sqrt(raiz);

        //Salida de datos
        cout.precision(3);
        cout<<"El resultado es: "<<raiz<<endl;


    //Elevar a x exponente, para ello haremos uso de la libreria: <math.h>

        float elevar, exponente;

        //Recogida de datos
        cout<<"Dime el numero a elevar: ";                      cin>>elevar;
        cout<<"Dime el exponenteal que lo quieres elevar: ";    cin>>exponente;
        
        //Calculos
        elevar = pow(elevar, exponente);        

        //Salida de datos
        cout.precision(3);
        cout<<"El resultado es: "<<elevar<<endl;

    return EXIT_SUCCESS;
}
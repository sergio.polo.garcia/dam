//Destructor (Elimina los objetos de una clase)

#include <iostream>
#include <stdlib.h>

using namespace std;

class Perro{
    private:
        string nombre, raza;
    public:
        Perro(string,string);   //Constructor
        ~Perro();               //Destructor
        void mostrar_datos();
        void jugar();
};

//Constructor
Perro::Perro(string _nombre, string _raza){

    nombre  =   _nombre;
    raza    =   _raza;
}

//Destructor
Perro::~Perro(){}

//Funciones (Metodos)
void Perro::mostrar_datos(){

    cout<<nombre<<"\t"<<raza<<endl;
}

void Perro::jugar(){
    cout<<nombre<<" está jugando"<<endl;
}


int main(int argc, char const *argv[])
{
    
    Perro p1("Max", "doberman");

    
    p1.mostrar_datos();
    p1.jugar();

    p1.~Perro();    //Destruir objeto

    return EXIT_SUCCESS;
}
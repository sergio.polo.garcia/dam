#include <iostream>
#include <stdlib.h>

using namespace std;

//Clases
class Persona{
    private:            //Atributos
        string nombre;
        int edad;

    public:             //Metodos
        Persona(string, int);
        ~Persona();
        void mostrar_persona();
};

class Empleado : public Persona{
    private:
        string codigo_empleado;

    public:
        Empleado(string, int, string);
        ~Empleado();
        void mostrar_empleado();
};

class Estudiante : public Persona {
    private:
        string codigo_estudiante;
    public:
        Estudiante(string, int, string);
        ~Estudiante();
        void mostrar_estudiante();
};

class Universitario : public Estudiante{
    private:
        string carrera;
    public:
        Universitario(string, int, string, string);
        ~Universitario();
        void mostrar_universitario();
};



//Constructores
Persona::Persona(string _nombre, int _edad){
    nombre  = _nombre;
    edad    = _edad;
}

Empleado::Empleado(string _nombre, int _edad, string _codigo_empleado) : Persona(_nombre,_edad){
    codigo_empleado = _codigo_empleado;
}

Estudiante::Estudiante(string _nombre, int _edad, string _codigo_estudiante) : Persona(_nombre, _edad){
    codigo_estudiante = _codigo_estudiante;
}

Universitario::Universitario(string _nombre, int _edad, string _codigo_estudiante, string _carrera) : Estudiante(_nombre, _edad, _codigo_estudiante){
    carrera = _carrera;
}


//Destructores
Persona::~Persona(){}
Empleado::~Empleado(){}
Estudiante::~Estudiante(){}
Universitario::~Universitario(){}


//Metodos
void Persona::mostrar_persona(){
    cout<<nombre<<" "<<edad<<" ";
}

void Empleado::mostrar_empleado(){
    mostrar_persona();
    cout<<codigo_empleado<<endl;
}

void Estudiante::mostrar_estudiante(){
    mostrar_persona();
    cout<<codigo_estudiante<<" ";
}

void Universitario::mostrar_universitario(){
    mostrar_estudiante();
    cout<<carrera<<endl;
}



int main(int argc, char const *argv[])
{
    
    Persona         p1("sergio",18);
    Empleado        e1("juan",19,"e85");
    Estudiante      es1("jose",15,"s12");
    Universitario   u1("pepe",21,"u12","filosofia");

    p1.mostrar_persona();
    e1.mostrar_empleado();
    es1.mostrar_estudiante();
    u1.mostrar_universitario();

    p1.~Persona();
    e1.~Empleado();
    es1.~Estudiante();
    u1.~Universitario();

    return EXIT_SUCCESS;
}

//Herencia en poo

#include <iostream>
#include <stdlib.h>

using namespace std;

//#############
//#   Clases  #
//#############

//Clase Padre
class Persona{
    private:        //Atributos
        string nombre;
        int edad;

    public:         //Metodos
        Persona(string, int);
        ~Persona();

        void mostrar_datos();
};

//Clase hija (tiene acceso a la parte publica de Persona)
class Alumno : public Persona{
    private:    //Atributos de la clase alumno
        string codigo_alumno;
        float nota_final;
    
    public:     //Metodos de la clase alumno
        Alumno(string, int, string, float);     //Constructor de Alumno, tb tiene lo que el constructor de persona    
        ~Alumno();

        void mostrar_alumno();
};


//Costructores
Persona::Persona(string _nombre, int _edad){
    nombre  = _nombre;
    edad    = _edad;
}

Alumno::Alumno(string _nombre, int _edad, string _codigo, float _nota) : Persona(_nombre, _edad){       //Asociamos los datos de Persona al constructor alumno
    codigo_alumno   = _codigo;
    nota_final      = _nota;
}

//Destructor
Persona::~Persona(){}

Alumno::~Alumno(){}


//Metodos
void Persona::mostrar_datos(){
    cout<<nombre<<" "<<edad<<endl;
}

void Alumno::mostrar_alumno(){
    mostrar_datos();
    cout<<codigo_alumno<<" "<<nota_final<<endl;
}




int main(int argc, char const *argv[])
{
    

    Persona p1("Sergio", 18); 
    Alumno a1("Sergio", 15, "a12", 7);

    p1.mostrar_datos();
    a1.mostrar_alumno();

    p1.~Persona();
    a1.~Alumno();
    
    return EXIT_SUCCESS;
}

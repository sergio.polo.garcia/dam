#include <iostream>
#include <stdlib.h>

using namespace std;

//Creacion de clase
class Persona{

    private:    //Atributos
        int edad;
        string nombre;

    public:     //Metodos
        Persona(int, string);   //constructor de la clase
        void leer();
        void correr();
};

//Constructor (inicializa los atributos)  
Persona::Persona(int _edad,string _nombre){
    edad = _edad;
    nombre = _nombre;
}



void Persona::leer(){
    cout<<"Soy "<<nombre<<" y estoy leyendo un libro"<<endl;
}
void Persona::correr(){
    cout<<"Soy "<<nombre<<" y estoy corriendo, tengo "<<edad<<" años"<<endl;
}



int main(int argc, char const *argv[])
{

    //Creacion de objetos
    Persona p1 = Persona(18,"Sergio");
    Persona p2(19,"Lucia");     //equivalente a lo de arriba
    Persona p3(21,"Juan");

    
    //Llamada a objetos
    p1.correr();
    
    p2.leer();
    
    p3.correr();
    p3.leer();

    return EXIT_SUCCESS;
}
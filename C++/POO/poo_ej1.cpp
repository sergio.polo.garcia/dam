/* Calculo de perimetro y area */ 

#include <iostream>
#include <stdlib.h>

using namespace std;

class Rectangulo{
	private:
		int largo, ancho;

	public:
		Rectangulo(int, int);
		void perimetro();
		void area();
};

//constructor
Rectangulo::Rectangulo(int _largo, int _ancho){
	largo = _largo;
	ancho = _ancho;
}



void Rectangulo::perimetro(){
	
	int per = 0;
	per = (largo*2) + (ancho*2);

	cout<<per<<endl;
}

void Rectangulo::area(){
	
	cout<<(largo*ancho)<<endl;
}



int main(int argc, char const *argv[])
{
	//Creacion de objeto
	Rectangulo r1(2,3);


	r1.perimetro();
	r1.area();


	return EXIT_SUCCESS;
}

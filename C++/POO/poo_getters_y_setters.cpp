/* Métodos constructores y modificadores (Getters y Setters) 


    setter -->  Para establecer/dar valores a los atributos (al igual que el constructor)
    getter -->  Para mostrar la informacion de los atributos encapsulados  

*/

#include <iostream>
#include <stdlib.h>

using namespace std;

class Punto{
    private:                //Atributos 
        int x,y;

    public:                 //Metodos
        Punto();                    //Constructor (Vacio pq usamos los getter y setter en su lugar)
        ~Punto();                   //Destructor

        void setPunto(int, int);    //Setter

        int getPuntoX();            //Getter del mismo tipo que el atrib X (int)
        int getPuntoY();            //Getter del mismo tipo que el atrib y (int)
};

//constructor
Punto::Punto(){

}

//Destructor
Punto::~Punto(){}

//###############
//#   setter    #
//###############
    void Punto::setPunto(int _x, int _y){

        x = _x;
        y = _y;
    }

//###############
//#   Getters   #
//###############

    int Punto::getPuntoX(){
        return x;
    }

    int Punto::getPuntoY(){
        return y;
    }

//###############


int main(int argc, char const *argv[])
{
    
    Punto p1;
    
    p1.setPunto(12,15);             //Establecemos valores gracias al setter
    
    cout<<p1.getPuntoX()<<endl;     //Salida de datos gracias al getter  
    cout<<p1.getPuntoY()<<endl;

    p1.~Punto();                    //Destructor

    return EXIT_SUCCESS;
}

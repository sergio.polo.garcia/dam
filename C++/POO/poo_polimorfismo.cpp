//Polimorfismo 
#include <iostream>
#include <stdlib.h>

using namespace std;

class Persona{
    private:
        string nombre;
        int edad;
    public:
        Persona(string,int);
        ~Persona();
        virtual void mostrar(); //Polimorfismo
};

class Alumno : public Persona{
    private:
        float nota_final;

    public:
        Alumno(string, int, float);
        ~Alumno();
        void mostrar();         //Polimosfismo
};

class Profesor : public Persona{
    private:
        string materia;
    public:
        Profesor(string, int, string);
        ~Profesor();
        void mostrar();         //Polimorfismo
};



//Constructores
Persona::Persona(string _nombre, int _edad){

    nombre  = _nombre;
    edad    = _edad;

}

Alumno::Alumno(string _nombre, int _edad, float _nota_final) : Persona( _nombre, _edad){

    nota_final = _nota_final;
}

Profesor::Profesor(string _nombre, int _edad, string _materia) : Persona( _nombre, _edad){

    materia     = _materia;
}

//Destructores
Persona::~Persona(){}
Alumno::~Alumno(){}
Profesor::~Profesor(){}

//Metodos
void Persona::mostrar(){
    cout<<nombre<<" "<<edad<<" ";
}

void Alumno::mostrar(){
    Persona::mostrar();         //Polimorfismo
    cout<<nota_final; 
}

void Profesor::mostrar(){
    Persona::mostrar();         //Polimorfismo
    cout<<materia;
}




int main(int argc, char const *argv[])
{


    Persona *vector[3];

    vector[0] = new Alumno("juan", 12, 7.3);
    vector[0] -> mostrar();

        cout<<"\n";

    vector[1] = new Persona("Sergio",18);
    vector[1] -> mostrar();

        cout<<"\n";

    vector[2] = new Profesor("Luis",25,"lengua");
    vector[2] -> mostrar();

        cout<<"\n";


    return EXIT_SUCCESS;
}
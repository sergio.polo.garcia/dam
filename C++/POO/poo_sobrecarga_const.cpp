// Sobrecarga de constructores
//El programa detecta los parametros introducidos (tipo) y elige que constructor usar   

#include <iostream>
#include <stdlib.h>

using namespace std;

//Clases
class Fecha{    
    private:
        int dia,mes,anno;

    public:
        Fecha(int,int,int);     //Constructor 1
        Fecha(long);            //Constructor 2 (sobrecarga)
        void mostrar_fecha();

};


//Constructor 1 (formato dd/mm/aaaa)
Fecha::Fecha(int _dia, int _mes, int _anno){

        dia     =   _dia;
        mes     =   _mes;
        anno    =   _anno;
}

//Constructor 2 (formato aaaammdd)
Fecha::Fecha(long fecha){
        anno    =   int(fecha/10000);               //Extraer año
        mes     =   int((fecha-anno*10000)/100);    //Extraer mes
        dia     =   int(fecha-anno*10000-mes*100);  //Extraer dia
}



void Fecha::mostrar_fecha(){

    cout<<dia<<"/"<<mes<<"/"<<anno<<endl;
}


int main(int argc, char const *argv[])
{
        
    Fecha f1(03,06,2022);
    Fecha f2(20220604);

    f1.mostrar_fecha();
    f2.mostrar_fecha();

    return EXIT_SUCCESS;
}
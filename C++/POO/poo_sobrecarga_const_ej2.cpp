/*Ejercicio 2: Construir una clase Tiempo que contenga Los siguientes atributos enteros: horas, minutos y segundos. 
Haz que la clase contenga 2 constructores, el primero debe tener 3 parámetros Tiempo(int, int, int) y el segundo sólo
tendrá un campo que serán los segundos y desensamble el número entero Largo en horas, minutos y segundos. */

#include <iostream>
#include <stdlib.h>

using namespace std;

//Clases
class Tiempo{
	private:
		int horas, minutos, segundos;
	public:
		Tiempo(int, int, int);
		Tiempo(long);
		void mostrar_tiempo();
};


//Constructor 1
Tiempo::Tiempo(int _horas, int _minutos, int _segundos){
	horas 		=	_horas;
	minutos 	=	_minutos;
	segundos 	=	_segundos;

}

//Constructor 2
Tiempo::Tiempo(long seg_totales){

	horas 		=	seg_totales/3600;
	seg_totales	%=	3600;	//Para restarle las horas
	minutos 	=	seg_totales/60;
	segundos 	= 	seg_totales%60; 

}

//Metodos
void Tiempo::mostrar_tiempo(){
	cout<<horas<<":"<<minutos<<":"<<segundos<<endl;
}



int main(int argc, char const *argv[])
{
	
	Tiempo hora_actual(9,54,52);
	Tiempo hora_seg(15611);


	hora_actual.mostrar_tiempo();
	hora_seg.mostrar_tiempo();


	return EXIT_SUCCESS;
}
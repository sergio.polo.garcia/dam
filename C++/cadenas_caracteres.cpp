#include <iostream>
#include <string.h>         //Libreria estandar para cadenas de caracteres
#include <stdlib.h>

using namespace std;

#define MAX 30

int main(int argc, char const *argv[])
{

    //Teoria basica
       
        //Declaracion de cadenas
        char cadena0[] = "Palabra";                                 //Elige el tamaño ella sola
        char cadena1[MAX] = {'P','a','l','a','b','r','a'};          //Lo mismo que lo de arriba, solo q con tamaño MAX(30)

        char cadena3[MAX];

        //Recogida de datos
        cout<<"dime tu nombre bb: ";    //cin>>cadena3; si usamos cin para meter datos a la cadena, cuando introduzan un espacio, cortará la cadena (/0)
        cin.getline(cadena3,MAX,'\n');  //Mejor forma de llenar la cadena cin.getline(nombre_cadena, tamaño_cadena, cuando_terminar(\n = darle al enter))


        //Salida de datos
        cout<<cadena0<<endl;
        cout<<cadena1<<endl;
        cout<<cadena3<<endl<<endl;


        //Conocer longitud de cadena de caracteres (celdas ocupadas, no disponibles)
        int longitud = 0;
        longitud = strlen(cadena3);

        cout<<"La longitud de la cadena es: "<<longitud<<endl<<endl;



        //Copiar contenido de una cadena a otra
        char cadena_old[MAX] = "texto a copiar";
        char cadena_new[MAX];

        strcpy(cadena_new,cadena_old);      //Primero cadena vacia y luego la llena (como en x = y)

        cout<<"El texto: "<<cadena_new<<". Se ha copiado correctamente"<<endl<<endl;


        //Comparar cadenas
        char cadena_a_comparar_1[MAX] = "a";
        char cadena_a_comparar_2[MAX] = "aa";

        float resultado_comparacion;
        
        resultado_comparacion = strcmp(cadena_a_comparar_1,cadena_a_comparar_2);       //Si son iguales, dara valor 0, si no lo son, dira que cadena es mas larga (si es la primera, arrojara un nº positvo. si es la segunda, uno negativo)

        cout<<resultado_comparacion<<endl;

            //Uso en condicional

            if (strcmp(cadena_a_comparar_1,cadena_a_comparar_2) != 0)
            {
                cout<<"Las cadenas no son iguales"<<endl;
                
                if (strcmp(cadena_a_comparar_1,cadena_a_comparar_2) > 0)
                {
                    cout<<"La primera cadena es mas larga que la segunda"<<endl;
                }

                if (strcmp(cadena_a_comparar_1,cadena_a_comparar_2) < 0)
                {
                    cout<<"La segunda cadena es mas larga que la primera"<<endl; 
                }                
            
            }

            if (strcmp(cadena_a_comparar_1,cadena_a_comparar_2) == 0)
            {
                cout<<"Las cadenas son iguales"<<endl<<endl;
            }

            //##################


        //Añadir al final de una cadena / Concatenar cadenas 
        int cant_cad_concat = 2;
        
        char cadena_a_concatenar_1[MAX] = "abcdefghijk";
        char cadena_a_concatenar_2[MAX] = "ABCDEFGHIJK";
        char cadena_concatenda[MAX * cant_cad_concat];

        strcpy(cadena_concatenda,cadena_a_concatenar_1);
        strcat(cadena_concatenda,cadena_a_concatenar_2);        //añade una cadena al final de la otra

        cout<<"\nLa cadena concatenada es: "<<cadena_concatenda<<endl<<endl;


    


    return EXIT_SUCCESS;
}
/* Paso de parámetros de tipo matriz + ejercicio elevar al cuadrado elementos de matriz c++ v1 Sergio Polo Garcia*/

#include    <iostream>

using namespace std;


#define nfilas  3
#define ncol    3


    void elevar_cuadrado (int matriz[][ncol]){                      //Poner siempre el numero de columnas (ncol) al hacer el paso de parametros

        for (int i = 0; i < nfilas; ++i)
        {
            for (int j = 0; j < ncol; ++j)
            {
                
                cout<<matriz[i][j] * matriz[i][j]<<"\t";

                if ((j+1)%nfilas == 0)                              //Coloca un \n cuando se llega al fin de la fila para asi embellecer el resultado mostrado en pantalla
                {
                    cout<<"\n";
                }
            }
        }
        cout<<"\n";
    }


int main(int argc, char const *argv[])
{

    int matriz[nfilas][ncol] = {{1,2,3},{4,5,6},{7,8,9}};

    elevar_cuadrado(matriz);

    
    return EXIT_SUCCESS;
}
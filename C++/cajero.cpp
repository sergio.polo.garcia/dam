/* Cajero c++ v1 Sergio Polo Garcia */

#include <iostream>
#include <math.h>


using namespace std;

//Ejercicio cajero automatico 

int proceso=1;                                                              //Para decidir cuando matar el programa
int saldo=1000, cantidad=0, opcion;   

int menu_principal(){
  
            cout<<"\n\n\t\t ############## Bienvenido a Bancos Polo ##############\n\n";

            cout<<"\t\t Su saldo es de: "<<saldo<<"€\n\n";                  //Muestra saldo de la cuenta

            cout<<"\t\t\t Que operación desea realizar:"    <<endl<<endl;
            cout<<"\t\t\t 1. Ingresar dinero"               <<endl;
            cout<<"\t\t\t 2. Retirada de efectivo"          <<endl;
            cout<<"\t\t\t 3. Salir porque es pobre"         <<endl;

            cout<<"\n\t\t\t\t"; cin>>opcion;                                //Lectura de opcion elegida

            return opcion;
}


//######## Salir ########
void salir(){
        cout<<"\n\n\t\t\t\t\t*****************\n";
        cout<<"\n\t\t\t\t\t* Vuelva pronto *\n";
        cout<<"\n\t\t\t\t\t*****************\n\n";
        proceso = 0;
}
//######################################


//######## Submenu ingresos (1) ########
int op_1 (){             

            cout<<"\n\n\t\t\t ############# Ingresar Dinero #############\n\n";

            cout<<"\t\t Su saldo es de: "<<saldo<<"€\n\n";              //Muestra saldo de la cuenta
                                
            cout<<"\t\t\t\t Seleccione cantidad a ingresar: "   <<endl;
            cout<<"\t\t\t\t 1. 20€"                             <<endl;
            cout<<"\t\t\t\t 2. 50€"                             <<endl;
            cout<<"\t\t\t\t 3. 70€"                             <<endl;
            cout<<"\t\t\t\t 4. 100€"                            <<endl;
            cout<<"\t\t\t\t 5. Otra"                            <<endl;
            cout<<"\t\t\t\t 6. Atras"                           <<endl;
                                
            cout<<"\n\t\t\t\t\t";           cin>>opcion;             //Lectura de opcion elegida        

        return opcion;
}

void ingreso(){
    saldo += cantidad;
    cout<<"Has elegido ingresar "<<cantidad<<"€, tu saldo actual es de: "<<saldo<<"€"<<endl;
    salir();
}
//######################################


//######## Submenu retirada (2) ########
int op_2 (){
            cout<<"\n\n\t\t\t ############# Retirar Dinero #############\n\n";

            cout<<"\t\t Su saldo es de: "<<saldo<<"€\n\n";
                     
            cout<<"\t\t\t\t Seleccione cantidad a retirar: "    <<endl;
            cout<<"\t\t\t\t 1. 20€"                             <<endl;
            cout<<"\t\t\t\t 2. 50€"                             <<endl;
            cout<<"\t\t\t\t 3. 70€"                             <<endl;
            cout<<"\t\t\t\t 4. 100€"                            <<endl;
            cout<<"\t\t\t\t 5. Otra"                            <<endl;
            cout<<"\t\t\t\t 6. Atras"                           <<endl;

            cout<<"\n\t\t\t\t\t";           cin>>opcion;             //Lectura de opcion elegida        

        return opcion;

}
void retirada(){
    saldo -= cantidad;
    cout<<"Has elegido retirar "<<cantidad<<"€, tu saldo actual es de: "<<saldo<<"€"<<endl;
    salir();
}
//######################################



int main(int argc, char const *argv[])
{

    while (proceso == 1){

    
        system("clear");                                                //Limpia la pantalla

        menu_principal();

        system("clear");


        //########## Redireccion a opcion elegida ##########

        switch (opcion){
            case 1:              
                op_1();                                                 //Submenu de la opcion 1

                system("clear");
                    
                    switch(opcion){
                                
                                case 1:
                                        cantidad=20;
                                        ingreso();
                                    break;

                                case 2:
                                        cantidad=50;
                                        ingreso();
                                    break;

                                case 3:
                                        cantidad=70;
                                        ingreso();
                                    break;

                                case 4:
                                        cantidad=100;
                                        ingreso();
                                    break;

                                case 5:
                                        cout<<"Escribe la cantidad a ingresar: ";
                                        cin>>cantidad;
                                       
                                        ingreso();
                                    break;

                                case 6:
                                        
                                    break;

                                  default: 
                                    cout<<"Error, introduzca un valor valido";
                                    break;    
                             }
                   
                break;
            

            case 2:
                op_2();                                                 //Submenu de la opcion 2
                system("clear");
                switch(opcion){
                                
                                case 1:
                                        cantidad=20;
                                        retirada();
                                    break;

                                case 2:
                                        cantidad=50;
                                        retirada();
                                    break;

                                case 3:
                                        cantidad=70;
                                        retirada();
                                    break;

                                case 4:
                                        cantidad=100;
                                        retirada();
                                    break;

                                case 5:
                                        cout<<"Escribe la cantidad a retirar: ";
                                        cin>>cantidad;
                                       
                                        retirada();
                                    break;

                                case 6:
                                        
                                    break;

                                  default: 
                                    cout<<"Error, introduzca un valor valido";
                                    break;    
                             }
                break;

            case 3: 
                salir();
            break;  
        }
    }


    return EXIT_SUCCESS;
}

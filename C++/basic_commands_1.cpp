/* Basic c++ v1 Sergio Polo Garcia */ 

#include <iostream>

using namespace std;                                //Para q funciona bien el cout y el cin 

int main(int argc, char const *argv[])
{


    // Tipos datos
        int entero = 15;
        float flotante = 10.454;  
        double floal_mas_grande = 10.5567756;
        char caracter = 'a';                        // comillas simples, 1 caracter para evitar overflow


        cout<<caracter<<"\n";                       //endl sustituye a \n


    // Salida de datos a pantalla 
        cout<<"Dime un numero :) \n";
    
    // Entrada de datos
        int numero;                                 // Declaracion de variable 
        cin>>numero;     
        cout<<"El numero introducido es: "<<numero<<"\n";

// ###################### Ejercicios ######################

    // Calculadora 2 numeros
        int num_calc_1, num_calc_2, suma = 0, resta = 0, multiplicaciom = 0, division = 0;

        // Recogida de datos
        cout<<"Dime dos numeros: \n";
        cin>>num_calc_1;
        cin>>num_calc_2;

        //Calculos
        suma = num_calc_1 + num_calc_2;
        resta = num_calc_1 - num_calc_2;
        multiplicaciom = num_calc_1 * num_calc_2;
        division = num_calc_1 / num_calc_2;

        //Salida con el resultado
        cout<<"Suma: "<<suma<<"\n";
        cout<<"Resta: "<<resta<<"\n";
        cout<<"Multiplicacion: "<<multiplicaciom<<"\n";
        cout<<"División: "<<division<<"\n";

    

    
    // Aplicar IVA a producto

        float precio_sin_iva, precio_con_iva, iva = 21;
        
        // Recogida de datos
        cout<<"Introduce el precio sin iva: ";
        cin>>precio_sin_iva;

        //Calculos
        precio_con_iva = precio_sin_iva + ((precio_sin_iva * iva) / 100); 

        //Salida con el resultado
        cout<<precio_con_iva<<"\n";

    


    // Pedir datos y echarlos por pantalla

        #define MAX 10

        int edad;
        char sexo[MAX]; // [MAX] para q lea 10 caracteres
        float altura;

        //Recogida de datos
        cout<<"Dime tu edad: ";                     cin>>edad;
        cout<<"Dime tu sexo: ";                     cin>>sexo;
        cout<<"Dime tu altura en metros: ";         cin>>altura;



        //Salida de datos
        cout<<"\n ########### \n\n";
        
        cout<<"Tu edad es? "<<edad<<"\n";
        cout<<"Tu sexo es? "<<sexo<<"\n";
        cout<<"Tu altura es "<<altura<< "m?"<<"\n";
        





    return EXIT_SUCCESS;
}
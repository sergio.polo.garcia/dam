/* Basic structures c++ v1 Sergio Polo Garcia */

/* ### Sintaxis ###

struct <nombreDeLaEstructura> {
<tipodeDato>    <nombreDeCampo>;
<tipoDeDato>    <nombreDeCampo>;
<tipoDeDato>    <nombreDeCampo>;
<tipoDeDato>    <nombreDeCampo>;

};      //Importante el ; al final
*/ 

#include    <iostream>
#include    <string.h>
using namespace std;

#define MAX 20


//# Ejemplo 1, introduciendo los datos directamente #

        struct persona{
            char    nombre[MAX];
            int     edad;
        }                                       //Declarando variables persona1 y persona2
            persona1 =  {"Sergio",18},          // , para separar variables al introducir los datos  
            persona2 =  {"Juan",20};


//# Ejemplo 2, usr introduciendo los datos #

        struct persona_usr{
            char nombre[MAX];
            int edad;
        }persona_usr1,persona_usr2;


int main(int argc, char const *argv[])
{

//# Salida de datos Ejemplo 1 #

    cout<<"Nombre 1: "<<persona1.nombre<<endl;
    cout<<"Edad 1: "<<persona1.edad<<endl<<endl;

    cout<<"Nombre 2: "<<persona2.nombre<<endl;
    cout<<"Edad 2: "<<persona2.edad<<endl;



//# Recogida de datos Ejemplo 2 #
    cout<<"Dime el nombre de la persona 1: ";   cin.getline(persona_usr1.nombre, MAX, '\n');
    cout<<"Dime la edad de la persona 1: ";     cin>>persona_usr1.edad;
        
    cout<<"\n";

    cout<<"Dime el nombre de la persona 2: ";   cin.getline(persona_usr2.nombre, MAX, '\n');        //Error, no pide el dato
    cout<<"Dime la edad de la persona 2: ";     cin>>persona_usr2.edad;

//# Salida de datos Ejemplo 2 #
    cout<<"\n\nEl nombre de la persona 1 es: "<<persona_usr1.nombre<<endl;
    cout<<"La edad de la persona 1 es: "<<persona_usr1.edad<<endl<<endl;

    cout<<"El nombre de la persona 2 es: "<<persona_usr2.nombre<<endl;
    cout<<"La edad de la persona 2 es: "<<persona_usr2.edad<<endl;


    return EXIT_SUCCESS;
}

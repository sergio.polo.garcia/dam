#include	<iostream>
#include	<string.h>
#include	<stdio.h>

using namespace std;

#define MAX 40
#define NUM_EMPLEADOS 2

	struct info_direccion{

		char	direccion[MAX];
		char	ciudad[MAX];
		char	provincia[MAX];
	};


	struct empleado{

		char	nombre[MAX];
		struct 	info_direccion dir_empleado;						//Estructura anidada, direccion_empleado = variable de la estructura info_direccion
		double	salario;
	}empleados[NUM_EMPLEADOS];										//Array de estructuras


int main(int argc, char const *argv[])
{
	
	//Recogida de datos

		for (int i = 0; i < NUM_EMPLEADOS; ++i)
		{
			cout<<"\n\n\t\t# DATOS DEL EMPLEADO"<<i+1<<" #\n\n";		//Titulo


			cout<<"\tNombre: ";		cin.getline(empleados[i].nombre, MAX, '\n');		
				
				//Introducir datos a estructura anidada
				cout<<"\tDireccion: ";		cin.getline(empleados[i].dir_empleado.direccion, MAX, '\n');		
				cout<<"\tCiudad: ";			cin.getline(empleados[i].dir_empleado.ciudad, MAX, '\n');
				cout<<"\tProvincia: ";		cin.getline(empleados[i].dir_empleado.provincia, MAX, '\n');

			cout<<"\tSalario: ";		cin>>empleados[i].salario;



			cin.get();		//Para vaciar buffer y evitar error de que no lea el nombre del empleado 2,3,4... ya que fflush no funciona y no esta diseñado para limpiar el stream de entrada, solo el de salida

			cout<<"\n";
		}


	//Salida de datos
		system("clear");

		cout<<"\n\n\t###############################";
		cout<<"\n\t#    CONFIRMACION DE DATOS    #";
		cout<<"\n\t###############################\n\n";
		

		for (int i = 0; i < NUM_EMPLEADOS; ++i)
		{
			cout<<"\n\n\t\t# EMPLEADO "<<i+1<<" #\n\n";					//Titulo

			cout<<"\tNombre: "<<empleados[i].nombre<<endl;		
			
				//Mostrar datos de estructura anidada
				cout<<"\tDireccion: "<<empleados[i].dir_empleado.direccion<<endl;
				cout<<"\tCiudad: "<<empleados[i].dir_empleado.ciudad<<endl;
				cout<<"\tProvincia: "<<empleados[i].dir_empleado.provincia<<endl;

			cout<<"\tSalario: "<<empleados[i].salario<<"€"<<endl<<endl;
		}





	return EXIT_SUCCESS;
}


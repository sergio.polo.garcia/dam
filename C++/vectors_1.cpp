/* Basic one-dimensional Vector/Array v1 Sergio Polo Garcia */

//Definition:   type name[size];

#include <iostream>

#define MAX 5

using namespace std;

int main(int argc, char const *argv[])
{


    //Ejercicio 1 suma de vectores
        
        //Definicion de variables
        int resultado_suma = 0;

        //Definir un vector de numeros
        int numeros[MAX] = {0,1,2,3,4};

        //Calculos
        for(int i=0; i<5; i++){
            resultado_suma += numeros[i];
        }

        //Salida de datos
        cout<<"El resultado es: "<<resultado_suma<<endl;



    //Ejercicio 2, leer de entrada estandar un vector de numeros

        //Definicion de variables
        int cant_numeros;
        //

        cout<<"Introduce la cantidad de numeros a guardar: ";   cin>>cant_numeros;              //Para establecer el tamaño del array
        int numeros_usr[cant_numeros];                                                          //Definicion del array


        //Recogida de datos + llenado del array
        for (int i = 0; i < cant_numeros; ++i)
        {
            cout<<"Dime un numero para intrucirlo en el array: ";
            cin>>numeros_usr[i];
        }


        //Salida de datos
        cout<<"Los valores introducidos son: ";
        for (int i=0; i < cant_numeros; ++i){
            cout<<numeros_usr[i];
        }        
        cout<<"\n";



    //Ejercicio 3, leer vector y decidir el numero mas alto

        int cant_numeros_3, numero_mayor = 0;
 
        //Recogida de datos
        cout<<"Introduce la cantidad de numeros a guardar: ";   cin>>cant_numeros_3;            //Para establecer el tamaño del array
        int numeros_3[cant_numeros_3];                                                          //Definicion del array

        for (int i = 0; i < cant_numeros_3; ++i)
        {
            cout<<"Dime un numero para intrucirlo en el array: ";
            cin>>numeros_3[i]; 
        
            //Algoritmo
            if (numeros_3[i] > numero_mayor)
            {
                numero_mayor = numeros_3[i];
            }

        }

        //Salida de datos
        cout<<"El numero mas alto es: "<<numero_mayor<<endl;



    // Unir vectores en un nuevo vector

        int cant_vectores = 2;                                                                  //Contiene la cantidad de vectores a unir

        //Creacion de vectores
        char vector_1[MAX] = {'a','b','c','d','e'};
        char vector_2[MAX] = {'A','B','C','D','E'};
        char vector_3[MAX * cant_vectores];

        //Algoritmo

        for (int i = 0; i < MAX; ++i){
            vector_3[i] = vector_1[i];
        }

        for (int i = 0+MAX; i < (MAX + MAX); ++i){
            vector_3[i] = vector_2[i - MAX];
        }

        
        //Salida de datos
        cout<<"El nuevo vector es: ";
        for (int i=0; i < MAX * cant_vectores; ++i){
            cout<<vector_3[i];
        }        
        cout<<"\n";



    return EXIT_SUCCESS;
}
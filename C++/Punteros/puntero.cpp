#include <iostream>

using namespace std;

/*

	&n = La direccion de n
	*n = La variable cuya direccion esta almacenada en n 

*/

int main(int argc, char const *argv[])
{
	
	int num, *dir_num;		// *dir_num = declaracion del puntero, ha de ser del mismo tipo que la variable a la que apunta

	num = 20;
	dir_num = &num;

	cout<<"Numero: "<<num<<endl;
	cout<<"Direccion de memoria de Numero: "<<&num<<endl<<endl;
	cout<<"Numero con Puntero: "<<*dir_num<<endl;
	cout<<"Direccion de memoria de Numero con Puntero: "<<dir_num<<endl;


	return EXIT_SUCCESS;
}
/*Ejercicio: Hallar el elemento maximo de un array*/

#include <iostream>
#define MAX 5

using namespace std;


int hallar_max(int *p_array){

	int mas_alto;

	for (int i = 0; i < MAX; i++){
		if (p_array[i] > mas_alto)
		{
			mas_alto = p_array[i];
		}
	}

	return mas_alto;
}




int main(int argc, char const *argv[])
{
	//Declaracion del array
	int array[MAX] = {0,1,8,18,4};


	cout<<hallar_max(array)<<endl;
	
	return EXIT_SUCCESS;
}
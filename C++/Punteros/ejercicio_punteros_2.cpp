/*Ejercicio 2: Rellenar un array de 10 numeros, posteriormente, utilizando punteros, indicar los pares o impares y su posicion en memoria. */

#include <iostream>

#define MAX 10
using namespace std;



int main(int argc, char const *argv[])
{
  
    int array[MAX] = {0,1,2,3,4,5,6,7,8,9};
    int *p_array;

    p_array = array;

    for (int i = 0; i < MAX; ++i)
    {
        switch(*p_array%2){
            case 0:
                cout<<"El número "<<*p_array<<" es par"<<endl;
                cout<<"\tSe aloja en la direccion de memoria"<<p_array<<endl<<endl;
                break;           
        }

        p_array++;
    }


    return EXIT_SUCCESS;
}

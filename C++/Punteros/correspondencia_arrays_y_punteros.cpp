#include <iostream>

using namespace std;

#define MAX 5

int main(int argc, char const *argv[])
{
    
    int numeros[MAX] = {0,0,2,3,4};
    int *p_numeros;


    p_numeros = numeros;                    // Es igual q "p_numeros = &numeros[0];" usamos la formula simplificada con el for ya que el array son posiciones contiguas de memoria

    for (int i = 0; i < MAX; ++i)
    {
        cout<<"Elemento ["<<i<<"] del vector: "<<*p_numeros++<<endl;        //*p_numeros++ para que una vez impreso el dato, aumente 4 bytes (tamaño de int) y pase a la siguiente celda de memoria
    }

    return EXIT_SUCCESS;
}
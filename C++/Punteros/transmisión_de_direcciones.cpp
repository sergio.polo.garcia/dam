/* Ejercico: intercambiar el valor de 2 variables con punteros*/

#include <iostream>

using namespace std;

void intercambio(float *p_num1, float *p_num2){         //float * indica que vamos a mandar 2 direcciones de variables de tipo float

    float aux;

    aux = *p_num1;
    *p_num1 = *p_num2;
    *p_num2 = aux;

}


int main(int argc, char const *argv[])
{
    float num1 = 25.8, num2 = 7.5;

    cout<<"Num1 = "<<num1<<endl;
    cout<<"Num2 = "<<num2<<endl;

    intercambio(&num1, &num2);

    cout<<"\n\t ### Tras el intercambio ###"<<endl;
    cout<<"Num1 = "<<num1<<endl;
    cout<<"Num2 = "<<num2<<endl;

    return EXIT_SUCCESS;
}
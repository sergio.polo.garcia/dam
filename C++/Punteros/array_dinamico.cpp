#include <iostream>
#include <stdlib.h>

using namespace std;

/* ### Asignacion dinamica de arrays ### 

new: Reserva el numero de bytes solicitado por La declaración. --> <stdlib.h>
delete : Libera un bloque de bytes reservado con anterioridad.

Ejemplo: Pedir al usuario n calificaciones y almacenarlos en un array dinamico

*/

//Global variables
int cant_notas, *p_notas;


void pedir_notas(){

    cout<<"Dime la cantidad de notas a registrar: ";
    cin>>cant_notas;

    p_notas = new int[cant_notas];          //Creacion del array de enteros (int), reservando memoria con new


    for (int i = 0; i < cant_notas; ++i){   //Llenado del array 
    
        cout<<"Introduce una nota: ";       cin>>p_notas[i];
    }
}


void mostrar_notas(){

    cout<<"\n\t### Notas introducidas ###"<<endl<<endl;
    
    for (int i = 0; i < cant_notas; i++){

        cout<<p_notas[i]<<endl;
        
    }
}


int main(int argc, char const *argv[])
{
    
    pedir_notas();
    mostrar_notas();

    delete[] p_notas;                       //Libera la memoria reservada anteriormente con el new

    return EXIT_SUCCESS;
}
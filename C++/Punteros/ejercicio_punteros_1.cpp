/* Ejercicio 1: Comprobar si un numero es par o impar, y seña lar La posicion de memoria donde se esta guardando el numero con punteros */

#include <iostream>

using namespace std;

//Recogida de datos
int preg_num(){
    int a; 

    cout<<"Dime un número: ";   
    cin>>a;

    return a;
}

//Calculos
void calculos (int *p_num){
    
    switch(*p_num%2){
        case 0:
            cout<<*p_num<<" es par"<<endl;
           break;

        case !0:
            cout<<*p_num<<" es impar"<<endl;
            break;
     }

    cout<<"Se ha guardado en la celda: "<<p_num<<" de la memoria RAM"<<endl;
}


int main(int argc, char const *argv[])
{
    int num = 0, *p_num;
    p_num = &num;                               //Guarda en el puntero la direccion de memoria de num

        num = preg_num();                       //Guarda el numero introducido por el usr
        calculos (p_num);

    return EXIT_SUCCESS;
}

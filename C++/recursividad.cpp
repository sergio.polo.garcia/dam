/* recursividad + facorial de un numero c++ v1 Sergio Polo Garcia*/

#include <iostream> 

using namespace std;

int factorial (int num){

    if (num == 0){                      //Caso base
        num = 1;
    }else{                              //Caso general
        num *= factorial(num-1);        //Recursividad, se llama a si misma
    }

    return num;
}


int main(int argc, char const *argv[])
{
    int num_usr;

        //Recogida de datos
        cout<<"Dime un numero: ";   cin>>num_usr;

        //Salida de datos
        cout<<"El factorial es: "<<factorial(num_usr)<<endl;


    return EXIT_SUCCESS;
}
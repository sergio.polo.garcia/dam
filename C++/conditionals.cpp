/* Conditionals and loops c++ v1 Sergio Polo Garcia*/

#include <iostream>
#include <stdlib.h>
#include <math.h>



using namespace std;

int main(int argc, char const *argv[])
{
    

    // If/else
            int num_if;

            //Recogida de datos
            cout<<"Dime un numero del 1 al 10: ";     cin>>num_if;

            //Condicional
            if (num_if >= 5)
            {
                cout<<"El numero es mayor/igual que 5"<<endl;
            }else{
                cout<<"El numero es menor que 5"<<endl;
            }

    cout<<"\n\n";    

    //Switch/case
        int num_switch;

        //Recogida de datos
        cout<<"Dime un numero del 1 al 5: ";     cin>>num_switch;        

        //Condicional
        switch(num_switch){
            case 1:
                cout<<"El numero introducido es el 1 \n";
                break;

            case 2:
                cout<<"El numero introducido es el 2 \n";
                break;

            case 3:
                cout<<"El numero introducido es el 3 \n";
                break;

            case 4:
                cout<<"El numero introducido es el 4 \n";
                break;

            case 5:
                cout<<"El numero introducido es el 5 \n";
                break;

            default:
                cout<<"Eres tan tonto que no sabes ni poner un numero ente el 1 y el 5 \n";
                break;

        }
    //While
        int i;
        i = 0;

        cout<<"\n";
        while (i < 10){

            cout<<i<<endl;

            i++;

        }


    //Do While
        cout<<"\n\n";

        i = 0;
        do{

            cout<<i<<". Do/While\n";
            i++;

        }while(i < 5);
            

    //For
        cout<<"\n\n";

        for (int i = 0; i < 5; ++i)
        {
            cout<<i<<". For\n";
        }

//#######################################
    
        cout<<"\n";

    //Generar numero aleatorio entre 1 y 100
        int num_aleatorio = 0;

        //Creacion del numero
        srand(time(NULL));                                          //Genera el nº aleatorio
        num_aleatorio = 1 + rand() % (100);                         //Guarda el nº en una variable, establecemos los valores minimo y maximo (1-100)

        //Salida de datos
        cout<<"El numero aleatorio es: "<<num_aleatorio<<endl;
    


    return EXIT_SUCCESS;
}
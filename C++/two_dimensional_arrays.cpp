/* Two-dimensional arrays in c++ v1 Sergio Polo Garcia*/ 

//Definition:   type name[n_filas][n_columnas];
#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    
        int n_filas, n_columnas, cant_datos;

        //##### Preguntar tamaño de la matriz #####
            cout<<" Cuantas filas quieres que tenga la matriz? ";        cin>>n_filas;
            cout<<" Cuantas columnas quieres que tenga la matriz? ";     cin>>n_columnas;


        //##### Creacion de la matriz #####
            int matriz[n_filas][n_columnas];


        //##### Llenar matriz con datos del usr #####
            cout<<"\n\n Introduce los valores de la matriz, ten en cuenta que cada "<<n_columnas<<" datos, se saltará a ña siguiente columna: "<<endl<<endl;
            
            for (int i = 0; i < n_filas; ++i){ 
        
                    for (int z = 0; z < n_columnas; ++z){
                        cout<<" Fila: "<<i<<" Columna: "<<z<<" ";     cin>>matriz[i][z];
                    }

             }

            cout<<"\n";
        //##### Salida de datos #####
            for (int i = 0; i < n_filas; ++i){

                for (int z = 0; z < n_columnas; ++z){
                    cout<<matriz[i][z]<<" ";    
                }
                cout<<"\n";                             //Para que se haga un salto de linea al llenar una fila
            }
            
            
            cout<<"\n\n";

    return EXIT_SUCCESS;
}

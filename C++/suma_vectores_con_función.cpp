/* Ejercicio suma_vectores_con_función c++ v1 Sergio Polo Garcia*/

#include    <iostream>
#define MAX 5


using namespace std;


    void suma_vectores (int vec1[], int vec2[]){

        for (int i = 0; i < MAX; ++i){

            cout<<vec1[i]+vec2[i]<<endl;
        }
    }


int main(int argc, char const *argv[])
{
    
    int vec1 [MAX] = {1,2,3,4,5};
    int vec2 [MAX] = {6,7,8,9,0};

    suma_vectores(vec1,vec2);


    return EXIT_SUCCESS;
}
/* Paso parametros pipo vector c++ v1 Sergio Polo Garcia*/

#include	<iostream>

using namespace std;

#define MAX 5 

	//Calcula el cuadrado del vector
	void cuadrado (int vector[]){				
		for (int i = 0; i < MAX; ++i)
		{
			vector[i] *= vector[i];
		}
	}

	void imprimir_resultado (int vector[]){
		for (int i = 0; i < MAX; ++i)
		{
			cout<<vector[i]<<endl;
		}
	}


int main(int argc, char const *argv[]){
	
	int vector [MAX] = {1,2,3,4,5};			//Declaracion del vector


	cuadrado(vector);
	imprimir_resultado(vector);


	return EXIT_SUCCESS;
}


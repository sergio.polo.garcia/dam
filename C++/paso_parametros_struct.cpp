/* paso_parametros_struct c++ v1 Sergio Polo Garcia*/

#include    <iostream>
#include    <stdlib.h>

using namespace std;

#define MAX 15


struct estructura {
        char    nombre[MAX];
        int     edad;
    }datos;


void pedir_datos (){

    cout<<"Dime tu nombre: "; 
    cin.getline(datos.nombre,MAX,'\n');                  //Rellenar struct tipo char

    cout<<"Dime tu edad: "; 
    cin>>datos.edad;
}

void mostrar_datos (){

    cout<<datos.nombre<<endl;
    cout<<datos.edad<<endl;
}


int main(int argc, char const *argv[])
{
    
    pedir_datos();
    mostrar_datos();

    return EXIT_SUCCESS;
}